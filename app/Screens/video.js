'use strict';

var React = require('react-native');
var Video = require('../component/Player');
import Store from 'react-native-store';


var {

    View,
    AsyncStorage

    } = React;

const DB = {
    '_videos': Store.model('_videos'),
}

module.exports = React.createClass({

    componentDidMount:function(){

        var data = this.props;

        DB._videos.find({
            where: {
                vid: data.vid ,
            }}).then(res => {
                if(!res)
                {
                    console.log("==>save db",res);

                    DB._videos.add({
                        vid: data.vid,
                        uname: data.uname,
                        time: data.time,
                        pic: data.pic,
                        name: data.name,
                        duration: data.duration,
                        click_count: data.click_count,
                        createtime:new Date()
                    });
                }


        });



    },
    render: function() {
        var url = 'http://101.200.79.153/wxbVideos/?r=site/m3u8&url=http://v.youku.com/v_show/id_'+this.props.vid+'.html';

        var sp = url.split(/data=/);
        if(sp.length>1)
        {
            var data = encodeURIComponent(sp[1]);
            url = sp[0]+'&data='+data;
        }


        return (
           <Video url={url} {...this.props}></Video>
        );
    },
});
