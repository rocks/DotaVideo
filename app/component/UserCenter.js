var React = require('react-native');

import {styles,userPicDef,BlurView,Icon,fontSize,colors,size} from '../common/Css';
import {Loading,LoadErr,BackButton} from '../common/Layout';
import Config from '../common/Config';
import Main from '../common/Main';
import ParallaxView from '../common/ParallaxView';
var {
    TouchableOpacity,
    Image,
    Text,
    View,
    ScrollView,
    ListView,
    Platform,
    } = React;



import {
    LazyloadListView,
    LazyloadView
} from 'react-native-lazyload';

import Store from 'react-native-store';
const DB = {
    '_videos': Store.model('_videos'),
}

var UserCenter = React.createClass({
    getInitialState:function(){

        return {
            pic:'http://wx.wefi.com.cn/images/bulr/Blur_02.jpg',

            userinfo:{
                email:'请登录'
            },
            subscribeList:[],
            histories:[],
            showTab:1,
            loading:false
        }
    },
    componentDidMount(){
        var userinfo = Main.getUserinfo(this);

        if (userinfo) {
            this.setState({
                userinfo:userinfo
            });

            Main.subscribeList(this,userinfo);

        }
        DB._videos.find({
            order:{
                createtime:'DESC'
            }
        }).then(res=>{
            if(res)
                this.setState({histories:res});
        });


    },
    _deleteHistory:function(vid){
        if(!vid)
            return;
        DB._videos.remove({
            where:{
                vid:vid
            }

        }).then(res=>{
            Main.toast("删除成功");
            DB._videos.find({
                order:{
                    createtime:'DESC'
                }
            }).then(res=>{
                this.setState({histories:res});
            });
        });
    },
    _rendUserInfo: function () {

        //console.log(this.state);
        return (

            <View style={[styles.HotContainer,{marginTop:this.state.marginTopAnim}]} ref="_rendUserInfo">
                        <View style={[styles.HotWidth,styles.aCenter]}>
                            <Image style={[styles.Hlogo]} source={require('../images/dota.png')}/>
                            <View style={[styles.Shadow]}>
                                <Image style={[styles.HAuthorImg]}
                                       source={{uri:this.state.pic}}/>
                            </View>
                            <Text style={[styles.Hauthor]}>{this.state.userinfo.email}</Text>
                            <Text style={[styles.Hname]}>{this.state.userinfo.email}</Text>
                            <View style={[styles.fRow,styles.Hfooter]}>
                                <TouchableOpacity style={[styles.UViconBox,styles.fRow,styles.UVborderMR]}
                                    onPress={()=>{this.setState({showTab:1})}}
                                >
                                    <Text style={[styles.textGreen]}>收藏</Text>
                                </TouchableOpacity>

                                <TouchableOpacity style={[styles.UViconBox,styles.fRow]}
                                     onPress={()=>{this.setState({showTab:2})}}

                                >
                                    <Text style={[styles.textGreen]}>历史</Text>
                                </TouchableOpacity>
                            </View>
                        </View>
                <TouchableOpacity onPress={() => {this.props.navigator.jumpBack()}} style={{position:"absolute",top:10,left:5}}>
                    <BackButton size={20}/>
                </TouchableOpacity>
            </View>

        )
    },
    _renderSubscribe:function(){
        var List = this.state.subscribeList.map((row,key)=>{
            try{
                var item = JSON.parse(row.content);
            }
            catch (e){
                console.log(e,row.content);
                return null;
            }
            if(row.status==0)
                return null;
            return (
                <TouchableOpacity
                    key={item.uid}
                    style={[styles.mainContainer,styles.borderBottom]}
                    {...this.props}
                    onPress={()=>{Main.goRouter(this,'UserVideo',item)}}


                >
                    <View style={[styles.Marginauto,styles.spaceBetween,styles.fRow]}>
                        <View style={[styles.UVLeft]}>
                            <Image style={[styles.UVImg,styles.jcenter,styles.aCenter]} source={{uri:item.upic}}>

                            </Image>
                        </View>
                        <View style={[styles.UVCenter,styles.fColumn]}>
                            <View style={[styles.fRow,styles.UVB]}>
                               <Text style={[{fontSize:fontSize.small,color:colors.success}]}>粉丝</Text>
                            </View>
                            <Text style={[styles.UVClick,{left:5}]}>{item.fans}</Text>
                            <Text style={[styles.UVName]}>{item.uname}</Text>
                            <Text style={[styles.UVdate,{height:11,overflow:'hidden'}]}>{item.intr}</Text>
                        </View>
                        <View style={[styles.UVRight,styles.fColumn,styles.tRight]}>
                            <View style={[styles.fRow,styles.fend]}>
                                <Text style={[styles.UVminute]}>播放</Text>

                                <Text style={[styles.UVtime]}>{item.click_count}</Text>
                            </View>


                        </View>
                    </View>
                </TouchableOpacity>
            );
        });
        return List;

    },
    _renderHistory(){
        var List = this.state.histories.map((item,key)=>{
            return (
                <View
                    key={key}
                    style={[styles.mainContainer,styles.borderBottom]}


                >
                    <View style={[styles.Marginauto,styles.spaceBetween,styles.fRow]}>
                        <TouchableOpacity style={[styles.UVLeft]}
                                          onPress={()=>{Main.goRouter(this,'Video',item)}}

                        >
                            <Image style={[styles.UVImg,styles.jcenter,styles.aCenter]} source={{uri:item.pic}}>
                                <View style={[styles.iconPlay,styles.itemCenter]}>
                                    <Icon name='ios-play-outline' size={22} color='#fff'/>
                                </View>
                            </Image>
                        </TouchableOpacity>
                        <View style={[styles.UVCenter,styles.fColumn]}>
                            <View style={[styles.fRow,styles.UVB]}>
                                <Icon name='ios-videocam-outline' size={16} color={'#e54847'}/>
                            </View>
                            <Text style={[styles.UVClick]}>{item.click_count}</Text>
                            <Text style={[styles.UVName]}>{item.name}</Text>
                            <Text style={[styles.UVdate]}>{item.time}</Text>
                        </View>
                        <View style={[styles.UVRight,styles.fColumn,styles.tRight]}>
                            <View style={[styles.fRow,styles.fend]}>
                                <Text style={[styles.UVtime]}>{item.duration}</Text>
                                <Text style={[styles.UVminute]}>秒</Text>
                            </View>
                            <TouchableOpacity style={[styles.seeBtn]}
                                onPress={()=>{this._deleteHistory(item.vid)}}
                            >
                                <Text style={[styles.seeText]}>删除</Text>
                            </TouchableOpacity>
                        </View>
                    </View>
                </View>
            );
        });
        return List;
    },
    render:function(){
        return (
        <ParallaxView
            backgroundSource={{uri:this.state.pic}}
            windowHeight={size.height/3}
            header={this._rendUserInfo()}
            scrollableViewStyle={{  }}
        >
            <View>
                {this.state.showTab==1 ? this._renderSubscribe() : this._renderHistory()}

                {this.state.loading ? <Loading size={20} text={'数据加载中'}/> : null}
            </View>
        </ParallaxView>


        );
    }
});


module.exports = UserCenter;
